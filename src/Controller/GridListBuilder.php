<?php


/**
 * @file
 * Contains \Drupal\ff_api\Controller\FFApiControllerProgramsV2.
 */

namespace Drupal\ff_grid\Controller;

use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Utility\Html;


class GridListBuilder extends ControllerBase {

    public function getList($entity_type, $bundle, $view_mode, $limit) {

        global $base_url;
        global $base_path;
        global $base_root;

        $build = array(
            '#markup' => '<div>Grid View</div><div ng-app="app" ng-controller="MainCtrl as results"><div id="grid1" ui-grid="results.gridDefinition" ui-grid-auto-resize ui-grid-resize-columns class="grid"></div></div>',
            '#attached' => array(
                'library' => array(
                    'ff_grid/grid',
                )
            )
        );

        $build['#attached']['drupalSettings']['ffGrid'] = array(
            'entityType' => $entity_type,
            'bundle' => $bundle,
            'viewMode' => $view_mode,
            'limit' => $limit
        );

        $build['#attached']['drupalSettings']['baseUrl'] = $base_url;
        $build['#attached']['drupalSettings']['basePath'] = $base_path;
        $build['#attached']['drupalSettings']['baseRoot'] = $base_root;

        return $build;

    }

    /**
     *
     * $route is just a key to look up our entity query and display definition.  This is just going to pass this value on to the API.
     *
     * @param $key
     * @return array
     */
    public function getListByRoute($key) {


//        $build = array(
//            '#markup' => '<div>Grid View</div><div ng-app="app" ng-controller="MainCtrl as results"><div id="grid1" ui-grid="results.gridDefinition" ui-grid-auto-resize ui-grid-resize-columns ui-grid-pinning ui-grid-expandable class="grid"></div></div>',
//            '#attached' => array(
//                'library' => array(
//                    'ff_grid/grid',
//                )
//            )
//        );

        $build = array(
            '#markup' => '<div ng-app="gridApp"><div id="grid1" curriculum-list-directive></div></div>',
            '#attached' => array(
                'library' => array(
                    'ff_grid/ff_grid_lib',
                )
            )
        );

        $build['#attached']['drupalSettings']['ffGrid'] = array(
            'key' => $key,
            'route' => $key,
            'modulePath' => drupal_get_path('module', 'ff_grid'),
            'entityType' => $key
        );

        return $build;

    }

}
