(function (angular, settings, location) {
    'use strict';

    var app = angular.module('gridApp');

    app.controller('curriculumListController', Controller);

    Controller.$inject = ['$q', '$scope', 'dataService'];
    function Controller($q, $scope, dataService) {

        var vm = this;

        var ws;

        vm.isBusy = false;

        var emptyArray = [];

        // for (var i = 0; i < 10; i++) {
        //     emptyArray.push([]);
        // }

        vm.filters = {};

        vm.data = emptyArray;
        vm.display = [];
        vm.hidden = [];
        vm.labels = [];

        var currentUrl = location.href.replace(location.origin, "");

        var entityBasePath = {
            'node' : '',
            'eck' : 'admin/structure/eck/entity'
        };

        var entityPath = (function() {
            if (entityBasePath[settings.ffGrid.entityType] === undefined) {
                return entityBasePath['eck'] + '/' + settings.ffGrid.entityType;
            } else {
                return settings.ffGrid.entityType;
            }
        })();

        vm.editPath = function(id) {
            return settings.path.baseUrl + entityPath + '/' + id + '/edit?destination=' + currentUrl;
        };

        vm.viewPath = function(id) {
            return settings.path.baseUrl + entityPath + '/' + id + '?destination=' + currentUrl;
        };

        $scope.$watch(
            function check() {
                return vm.filters;
            },
            function changed(newValue, oldValue) {
                if (!angular.equals(newValue, oldValue)) {
                    updateFilters();
                }
            },
            true
        );

        this.$onInit = function() {

            vm.isBusy = true;

            console.log('isBusy', vm.isBusy);

            dataService.getEntities(0).then(
                function success(response) {
                    console.log('getEntities:: ', response);
                    ws = response.data;

                    updateVm();
                    updateFilters();
                    getPages(response.data.pages);

                }
            );

            return vm;
        };

        function getPages(pages) {

            var requests = [];
            for (var i = 1; i <= pages.page_end; i++) {
                // queue the rest of the queries.
                requests.push(
                    dataService.getEntities(i).then(
                        function success(response) {
                            ws.data = ws.data.concat(response.data.data);
                            updateVm();
                            updateFilters();
                        }
                    )
                );
            }

            $q.all(requests).then(
                function success(datas) {
                    vm.isBusy = false;
                }
            )
        }

        function updateVm() {

            vm.data = ws.data;
            vm.display = ws.display;
            vm.hidden = ws.hidden;
            vm.labels = ws.labels;

            vm.filteredData = vm.data.filter(function(obj) {
                for (var i = 0; i < vm.filters.length; i++) {
                    var fieldName = vm.filters[i];
                }

                return obj;
            });
            console.log(vm);
        }

        function updateFilters() {

            var filterKeys = Object.keys(vm.filters);
            var filters = vm.filters;

            if (filterKeys.length === 0) {
                return true;
            }

            // See if we have any filters
            for (var i = 0; i < filterKeys.length; i++) {
                var filter = vm.filters[filterKeys[i]];
                if (filter.length === 0) {
                    delete(filters[filterKeys[i]]);
                }
            }

            if (Object.keys(filters).length === 0) {
                vm.filteredData = vm.data;
            } else {
                vm.filteredData = vm.data.filter(function(obj) {
                    var matches = 0;

                    var filterKeys = Object.keys(filters);

                    // If there is something in the string, it needs to meet both

                    var l = filterKeys.length;

                    for (var i = 0; i < filterKeys.length; i++) {
                        var filter = filters[filterKeys[i]];
                        var fieldName = filterKeys[i];

                        var re = new RegExp(filter);

                        for (var ii = 0; ii < obj[fieldName].length; ii++) {
                            var match = re.test(obj[fieldName][ii].value);
                            if (match) {
                                matches++;
                            }
                        }
                    }

                    return matches === l;
                });
            }

        }
    }
})(window.angular, window.drupalSettings, window.location);
