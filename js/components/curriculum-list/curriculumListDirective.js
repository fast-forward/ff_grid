(function (angular, settings) {
    'use strict';

    var app = angular.module('gridApp');
    app.directive('curriculumListDirective', Directive);

    Directive.$inject = [];
    function Directive() {
        return {
            restrict: 'A',
            link: {},
            scope: {},
            controller: 'curriculumListController',
            controllerAs: 'list',
            bindToController: true,
            templateUrl: settings.path.baseUrl + settings.ffGrid.modulePath + '/js/components/curriculum-list/curriculum-list.html'
        };
    }
})(window.angular, window.drupalSettings);