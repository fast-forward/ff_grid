(function (angular, ffGrid) {
    'use strict';

    var app = angular.module('gridApp');

    app.factory('dataService', Service);

    Service.$inject = ['$q', '$http'];
    function Service($q, $http) {

        var pub = {
            getEntities: getEntities
        };

        // TODO: Get this from drupalSettings current URL
        var baseUrl = '/gradapi/';

        return pub;

        function getEntities(page) {

            if (page === undefined) {
                page = 0;
            }

            var endpoint;

            // TODO: Move to WS and just return the endpoint in DrupalSettings
            if (ffGrid.route !== undefined) {
                endpoint = 'ws/entity-list-by-route/' + ffGrid.route + '/' + page;
            } else {
                endpoint = 'ws/entity-list/' + ffGrid.entityType + '/' + ffGrid.bundle + '/' + ffGrid.viewMode + '/' + ffGrid.limit;
            }
            return _get(endpoint, {
                cache: true
            });
        }

        //Private Methods
        function _get(endpoint, config) {
            return httpRequest('get', endpoint, config);
        }
        function _post(endpoint, config) {
            // $http.defaults.headers.post = {"Content-Type":"raw"};
            return httpRequest('post', endpoint, config);
        }

        function _put(endpoint, config) {
            return httpRequest('put', endpoint, config);
        }

        function _patch(endpoint, config) {
            return httpRequest('patch', endpoint, config);
        }

        function _head(endpoint, config) {
            return httpRequest('head', endpoint, config);
        }

        function _delete(endpoint, config) {
            return httpRequest('delete', endpoint, config);
        }

        function httpRequest(type, endpoint, config) {

            //create http request
            var httpConfig = {
                method: type.toUpperCase(),
                url: baseUrl + endpoint,
                params: config.params,
                data: config.data,
                withCredentials: true
            };

            return $http(httpConfig).then(
                function success(response) {
                    return response;
                },
                function failure(response) {
                    return response;
                }
            );
        }
    }

})(window.angular, drupalSettings.ffGrid);
