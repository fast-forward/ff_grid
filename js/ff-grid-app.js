/**
 *
 * @description Base App file
 *
 * 'ui.grid.expandable', 'ui.grid.selection', 'ui.grid.pinning'
 */
(function (angular, settings, location, moment) {
    'use strict';

    var app = angular.module('app', ['ui.grid', 'ui.grid.autoResize', 'ui.grid.resizeColumns', 'ui.grid.selection', 'ui.grid.pinning', 'ui.grid.expandable']);

    app.controller('MainCtrl', Controller);

    Controller.$inject = ['dataService', '$scope'];
    function Controller(dataService, $scope) {

        var vm = this;

        var ws;

        vm.data = [];

        var display;

        var currentUrl = location.href.replace(location.origin, "");

        dataService.getEntities().then(
            function success(response) {
                console.log('getEntities:: ', response);
                ws = response.data;
                updateVm();
            }
        );

        vm.gridDefinition = {
            enableSorting: true,
            enableFiltering: true
        };

        var entityBasePath = {
            'node' : '',
            'eck' : 'admin/structure/eck/entity'
        };

        var entityPath = (function() {
            if (entityBasePath[settings.ffGrid.entityType] === undefined) {
                return entityBasePath['eck'] + '/' + settings.ffGrid.entityType;
            } else {
                return settings.ffGrid.entityType;
            }
        })();

        // TODO: Move this into the API instead of here.
        var idField = (function() {

            switch(settings.ffGrid.entityType) {
                case 'eck':
                    return 'id[0].value';
                    break;
                case 'node':
                    return 'nid[0].value';
                    break;
                case 'taxonomy':
                    return 'tid[0].value';
                default:
                    return 'id[0].value';
            }
        })();


        return vm;

        function updateVm() {

            vm.gridDefinition.columnDefs = [];

            var columns = [
                {
                    name : 'Edit',
                    field : idField,
                    cellTemplate: '<div class="ui-grid-cell-contents"><a href="' + settings.path.baseUrl + entityPath + '/{{ COL_FIELD }}/edit?destination=' + currentUrl + '" >Edit</a></div>',
                    weight : -99999,
                    enableFiltering : false,
                    enableSorting: false
                },
                {
                    name : 'View',
                    field : idField,
                    cellTemplate: '<div class="ui-grid-cell-contents"><a href="' + settings.path.baseUrl + entityPath + '/{{ COL_FIELD }}?destination=' + currentUrl + '" >View</a></div>',
                    weight : -99999,
                    enableFiltering : false,
                    enableSorting: false
                }
            ];

            angular.forEach(ws.display, function(value, key) {

                var columnDef = {
                    name : ws.labels[key],
                    field : key,
                    cellFilter : 'arrayJoin',
                    enableFiltering : true,
                    filterCellFiltered : true,
                    weight : value.weight
                };

                if (ws.display[key].type === 'timestamp_ago') {
                    columnDef.field = key + '[0].value';
                    columnDef.cellFilter = 'timestamp_ago';
                    // columnDef.enableFiltering = false;
                }

                if (ws.display[key].type === 'timestamp') {
                    columnDef.field = key + '[0].value';
                    columnDef.cellFilter = 'timestamp';
                    // columnDef.enableFiltering = false;
                }

                this.push(columnDef);
            }, columns);


            columns = columns.sort(function(a, b) {
                return parseFloat(a.weight) - parseFloat(b.weight);
            });

            vm.gridDefinition.columnDefs = columns;

            vm.gridDefinition.data = ws.data;
        }
    }

})(window.angular, window.drupalSettings, window.location, window.moment);