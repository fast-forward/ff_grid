(function (angular, moment) {

    var app = angular.module('gridApp', []);

    app.filter('timestamp_ago', function () {

        return function (value) {

            if (value === undefined) {
                return undefined;
            }

            var m = moment.unix(value);
            var diff = m.diff(moment.now(), days);

            if (diff > -30) {
                return m.format('YYYY-MM-DD hh:mm a');
            } else {
                return m.fromNow();
            }
        }
    })

    app.filter('timestamp', function () {

        return function (value) {
            if (value === undefined) {
                return undefined;
            }
            var m = moment.unix(value);
            return m.format('YYYY-MM-DD hh:mm a');
        }
    })

})(window.angular, window.moment);