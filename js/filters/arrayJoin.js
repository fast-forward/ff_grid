(function (angular) {

    var app = angular.module('gridApp', []);

    app.filter('arrayJoin', function() {

        return function(value) {

            if (value === undefined) {
                return undefined;
            }
            var arr = [];

            for (var i = 0; i < value.length; i++) {
                arr.push(value[i].value);
            }

            return arr.join(', ');
        }
    });

})(window.angular);

